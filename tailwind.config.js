/*
** TailwindCSS Configuration File
**
** Docs: https://tailwindcss.com/docs/configuration
** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
*/
module.exports = {
    theme: {
        borderRadius: {
            'none': '0',
            'sm': '.125rem',
            'md': '0.375rem',
            default: '.25rem',
            'lg': '.5rem',
            'xlg': '.8rem',
            'full': '9999px',
        },
        extend: {
            colors: {
                cyan: {
                    '100': '#BEF8FD',
                    '200': '#87EAF2',
                    '300': '#54D1DB',
                    '400': '#38BEC9',
                    '500': '#2CB1BC',
                    '600': '#14919B',
                    '700': '#0E7C86',
                    '800': '#0A6C74',
                    '900': '#044E54',
                    '50': '#E0FCFF',
                    default: '#E0FCFF'
                },
                gray: {
                    '100': '#D9E2EC',
                    '200': '#BCCCDC',
                    '300': '#9FB3C8',
                    '400': '#829AB1',
                    '500': '#627D98',
                    '600': '#486581',
                    '700': '#334E68',
                    '800': '#243B53',
                    '900': '#102A43',
                    '50': '#F0F4F8',
                    default: '#F0F4F8'
                },
                indigo: {
                    '100': '#BED0F7',
                    '200': '#98AEEB',
                    '300': '#7B93DB',
                    '400': '#647ACB',
                    '500': '#4C63B6',
                    '600': '#4055A8',
                    '700': '#35469C',
                    '800': '#2D3A8C',
                    '900': '#19216C',
                    '50': '#E0E8F9',
                    default: '#E0E8F9'
                },
                pink: {
                    '100': '#FAB8D9',
                    '200': '#F191C1',
                    '300': '#E668A7',
                    '400': '#DA4A91',
                    '500': '#C42D78',
                    '600': '#AD2167',
                    '700': '#9B1B5A',
                    '800': '#781244',
                    '900': '#5C0B33',
                    '50': '#FFE0F0',
                    default: '#FFE0F0'
                },
                red:
                    { '100': '#FACDCD',
                        '200': '#F29B9B',
                        '300': '#E66A6A',
                        '400': '#D64545',
                        '500': '#BA2525',
                        '600': '#A61B1B',
                        '700': '#911111',
                        '800': '#780A0A',
                        '900': '#610404',
                        '50': '#FFEEEE',
                        default: '#FFEEEE' },
                yellow:
                    { '100': '#FCEFC7',
                        '200': '#F8E3A3',
                        '300': '#F9DA8B',
                        '400': '#F7D070',
                        '500': '#E9B949',
                        '600': '#C99A2E',
                        '700': '#A27C1A',
                        '800': '#7C5E10',
                        '900': '#513C06',
                        '50': '#FFFAEB',
                        default: '#FFFAEB' }
            },
            fontSize: {
                '2xs': '.5rem',
                '1-5xl': '1.38rem',
                '2-5xl': '1.75rem',
                '3-5xl': '2rem',
                '3-7xl': '2.13rem',
                '4-5xl': '2.75rem',
                '5-5xl': '3.5rem',

                //text-2xs is       8px at 1920 wide
                //text-xs  is       12px at 1920 wide
                //text-sm is        14px at 1920 wide
                //text-base is      16px at 1920 wide
                //text-lg is        18px at 1920 wide
                //text-xl is        20px at 1920 wide
                //text-1.5xl is     22px at 1920 wide
                //text-2xl is       24px at 1920 wide
                //text-2.5xl is     28px at 1920 wide
                //text-3xl is       30px at 1920 wide
                //text-3.5xl is     32px at 1920 wide
                //text-3.7xl is     34px at 1920 wide
                //text-4xl is       36px at 1920 wide
                //text-4.5xl is     40px at 1920 wide
                //text-5xl is       48px at 1920 wide
                //text-5.5xl is     60px at 1920 wide
                //text-6xl is       64px at 1920 wide
            }
        }
    },
    variants: {},
    plugins: []
};
