export default function ({ store, redirect, $auth }) {
    let user = $auth.loggedIn;

    // If the user is not authenticated
    if (!user) {
        return redirect('/login')
    }
}
