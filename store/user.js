import axios from 'axios';
import ls from 'local-storage';

export const state = () => ({
    currentUser: {},
    token: ls.get('token') || null,
});

export const mutations = {
    SET_USER (state, payload) {
        state.currentUser = payload
    },
    SET_TOKEN(state, payload){
        state.token = payload;
    },
    CLEAR(state){
        state.list = [];
    },
    DESTROY_TOKEN(state, payload){
        state.token = null;
    }
};

export const getters = {
    loggedIn(state){
        return state.token !== null;
    },
    getToken(state){
        // console.log('getToken', state.token)
        return state.token;
    }
};

export const actions = {
    fetchUser({ commit }){
        console.log('fetch user');

        // axios.defaults.headers.common['Authorization'] = 'Bearer ' + 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiYzMzYzUyNTFiNjUzMTg4NzhhMzk5YzI2ZmE1NTM4YTY3MjA1MjQ1MGYzMDM4NjU3MmQ5YjQwNzExOTdmZDQwZTRlNTBlNTMwMmJiYTQwMmYiLCJpYXQiOjE1NzY2ODU4MTYsIm5iZiI6MTU3NjY4NTgxNiwiZXhwIjoxNjA4MzA4MjE2LCJzdWIiOiIxMyIsInNjb3BlcyI6WyIqIl19.k0aRmKCLYGEbstcxfGKhzswhJ8x12Zcv1mRvKSRr5r-i-W0plmWm5y-yEkMIla5w3IwEE8JBuPxkHEtNuH9YGFBTIFbmCwKOzGedbbdY6N0docY3drHpg74tlGSl4DeGOkzMdeRg8iEnMIbPsHgOY9ANXEjUX02uZW09YrxFGvPA1fxfC62ONFB7Bp8Hvztde2wMzBSRatvfBqvHbzCLB26iDyNaCb_Yv_wHPsr34JOG-1b_lUf6dpSIJtFD3r-SBvhr8mrdR-WjEcAsasNvYJDuy9p_N5BkiG-PK-fy28KdBQs0lyNHGDMnhGOhm0SbRjDXP3ggQ53TEUicF5063STJofWStVVzlY_GpPlyC3rHoH-HVCqYrXw6NOCNZnnoSrXyvJd2Upkcirv3Gz38Kd-koPQy9S5PexVs9GxexPDvWVCHHtMH06iJJYe1CMOT0InH_84d10okUYipo-VXb1wuSJDI7S26SLvnrU_htCN9dtthCKfre1mtaQGqf3sw9cPJEkNs-LwfwaNQmXLB-8c1m2U1fiBxInjQ20-FmmYi2thSeUHap9FphjIxrxW54nvU6qaAB6ZwntvK26Yq1wmHGIkGoj9aNLe_2RXHFQqCjwB8_RTj0tbKf5D0mq5_bjKwAhMvVq0Vk55-vicWx_UvhY1CDI7yxQdToLvLR6E';
        // return new Promise((resolve, reject) => {
        //     axios.get(process.env.apiUrl + '/api/user')
        //         .then((data) => {
        //
        //             commit('SET_USER', data.data);
        //             commit('SET_TOKEN', ls.get('token'));
        //
        //             resolve(data);
        //         }).catch((err) =>{
        //             reject(err);
        //     });
        // });

    },
    register({commit}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.$post('/register', {
                name: payload.name,
                email: payload.email,
                password: payload.password
            }).then((res) => {

                resolve(res);
            }).catch(error => {
                console.log(error);
                reject(error)
            });
        });
    }
};
