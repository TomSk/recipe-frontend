import _ from 'lodash';
import Moment from 'moment';
import { extendMoment } from 'moment-range';

const moment = extendMoment(Moment);

export const state = () => ({
    startDate: new Date(),
    days: [],
    plannerRecipes: [],
    range: '5'
});

export const getters = {
    getIngredientList(state, getters, rootState ){
        let list = state.plannerRecipes.map(el => {
            return el.content['ingredients']
        });

        list = _.flattenDeep(list);

        let ingredientNames = _.uniq(list.map(el => el['name']));


        let ingredientAmount = ingredientNames.map((el) => {

            let nameList = list.filter((item) => item.name == el);
            let priceList = rootState.ingredients.ingredients.filter((item) => {
                if(item.name == el){
                    return item.price;
                }
            });

            let total = nameList.map(el => el.amount).reduce((a,b) => parseFloat(a) + parseFloat(b));
            let price =  priceList.map(el => el.price);

            if(price.length > 0){
                price = price[0]
            } else {
                price = 0;
            }

           return {
                'name':el,
                'amount': total,
                'price': (price * total).toFixed(2)
            };

            return el;
        });

        if(ingredientAmount && ingredientAmount.length){
            return ingredientAmount
        }
    },
    getPlannerRecipes(state){
        return state.plannerRecipes
    },
    getDaysOfWeek(state){

        let start = moment(state.startDate);
        let end = moment(state.startDate).add(state.range -1, 'days')

        const range = moment.range(start, end);

        let dates = [];

        for (let day of range.by('day')) {
            dates.push( day.format('ddd'));
        }

        return dates.splice( 0, 7);
    }
};

export const mutations = {
    ADD_RECIPE(state, payload){
        let findId = state.plannerRecipes.filter(el => el._id == payload._id);

        if(findId.length > 0){
            state.plannerRecipes.find(el => el._id === payload._id).content = payload.content;
        } else {
            state.plannerRecipes.push(payload);
        }
    },
    DELETE_RECIPE(state, id){
        state.plannerRecipes = state.plannerRecipes.filter(el => el._id != id);

        console.log(id, state.plannerRecipes)
    },
    CLEAR_RECIPES(state){
        state.plannerRecipes = [];
    },
    SET_RECIPES(state, payload){
        state.plannerRecipes = payload;
    },
    SET_RANGE(state, payload){
        state.range = payload;
    },
    SET_DAYS(state, payload){
        state.days = payload;
    },
    SET_START_DATE(state, payload){
        state.startDate = payload;
    },
    UPDATE_RECIPES(state){
        state.plannerRecipes.map((el, index) => {
            el._id = state.days[index].id;
        });
    }
};


export const actions = {
    generateList({commit, state}, payload){
        let newList = [];
        //
        // state.days.map((day) => {
        //     let item = payload[Math.floor(Math.random() * payload.length)];
        //
        //     newList.push({
        //         _id: day.id,
        //         content: item
        //     });
        // });
        //


        return new Promise((resolve, reject) => {
            this.$axios.$get('/generate-recipe-list/' + state.days.length).then((res) => {
                resolve(res);

                state.days.map((day, index) => {

                    console.log(res[index])

                    if(res[index]){
                        newList.push({
                            _id: day.id,
                            content: res[index]
                        });
                    }
                });

                commit('SET_RECIPES', newList)

            }).catch(error => {
                reject(error);

            });
        });
    },
    setRange({commit, state}, payload){

        let start = moment(state.startDate);
        let end = moment(state.startDate).add(payload-1, 'days')

        const range = moment.range(start, end);

        let dates = [];

        for (let day of range.by('day')) {
            dates.push({
                name: day.format('dddd'),
                id: day.format('dddd') + '-' + day.format('DD-MM-YYYY'),
                date: day.format('DD-MM-YYYY')
            });
        }

        commit('SET_RANGE', payload);
        commit('SET_DAYS', dates);
    },
    updateRange({commit, getters, state}, payload){

        let start = moment(state.startDate);
        let end = moment(state.startDate).add(state.range-1, 'days')

        const range = moment.range(start, end);

        // let days = state.days;

        let dates = [];


        for (let day of range.by('day')) {
            dates.push({
                name: day.format('dddd'),
                id: day.format('dddd') + '-' + day.format('DD-MM-YYYY'),
                date: day.format('DD-MM-YYYY')
            });
        }

        // console.log('plannerRecipes 2', state.plannerRecipes)

        commit('SET_DAYS', dates);
        commit('UPDATE_RECIPES');

    },
    setStartDate({commit, dispatch, state}, payload){
        commit('SET_START_DATE', payload)

        dispatch('updateRange')
    }
};
