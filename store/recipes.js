import axios from 'axios';
import ls from 'local-storage';

export const state = () => ({
    list: [],
    errors: {
        message: '',
        fields: {},
        showMsg: false
    },
    searchTerm: '',
});

export const getters = {
    filteredRecipes(state){

        let filterList;

        if(state.searchTerm && state.searchTerm.length > 0){
            filterList = state.list.filter(el => {
                return el.title.includes(state.searchTerm)
            });
        } else {
            filterList = state.list;
        }

        return filterList;
    }
};

export const mutations = {
    SET_RECIPES(state, payload){
        state.list = payload;
    },
    SET_ERRORS(state, payload){
        state.errors.fields = payload.errors;
        state.errors.message = payload.message;
        state.errors.showMsg = true;
    },
    CLEAR_ERRORS(state){
        console.log('CLEAR_ERRORS')
        state.errors.showMsg = false;
    },
    SET_SEARCH_TERM(state, payload){
        state.searchTerm = payload;
    },
    SORT_RECIPES(state, payload){

        if(payload == 'asc'){
            state.list = state.list.sort((a,b) => {
                return new Date(b.created_at) - new Date(a.created_at)
            });

        } else if(payload == 'desc'){
            state.list = state.list.sort((a,b) => {
                return new Date(a.created_at) - new Date(b.created_at)
            });

        } else if(payload == 'rating-asc'){
            state.list = state.list.sort((a,b) => {
                return b.rating - a.rating;
            });

        } else if(payload == 'rating-desc'){
            state.list = state.list.sort((a,b) => {
                return a.rating - b.rating;
            });

        } else {
            state.list = state.list.sort((a,b) => {
                return new Date(a.created_at) - new Date(b.created_at)
            });
        }
    }

};


export const actions = {
    addRecipe({commit, dispatch}, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.$post('/add-recipe', payload)
                .then((res) => {
                    dispatch('getRecipes');

                    resolve(res);
                }).catch((error) => {
                    commit('SET_ERRORS', error.response.data)

                    reject(error);
                });
        });
    },
    getRecipes({commit, state, rootState}){

        return new Promise((resolve, reject) => {
            this.$axios.$get('/show').then((res) => {

                // console.log('show', res)

                commit('SET_RECIPES', res);
                resolve(res);

            }).catch(error => {

                console.log(error);
                reject(error);

            })
        });
    },
    getSingleRecipe({commit, state, rootState}, payload){

        return new Promise((resolve, reject) => {
            this.$axios.$get('/recipe/' +  payload.id).then((res) => {
                resolve(res);

            }).catch(error => {
                reject(error);

            });
        });
    },
    deleteRecipe({commit, dispatch, state, rootState}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.$post('/delete-recipe/' + payload.id, payload)
                .then((res) => {
                    dispatch('getRecipes').then(() => {
                        resolve(res);
                    });
                }).catch(error => {
                    console.log(error);
                    reject(error)
                });
        });
    },
    editRecipe({commit, dispatch, state, rootState}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.$post('/edit-recipe/' + payload.id, payload)
                .then((res) => {
                    dispatch('getRecipes').then(() => {
                        resolve(res);
                    });

                }).catch(error => {
                    console.log(error);
                    reject(error)
                });
        });
    },
    setSearchTerm({commit}, payload){
        commit('SET_SEARCH_TERM', payload);
    }
};
