import _ from "lodash";

export const state = () => ({
    ingredients: [
        // {
        //     id: 0,
        //     name: 'Onions',
        //     price: '£1.56',
        //     size: '2kg',
        // },
        // {
        //     id: 2,
        //     name: 'Potatoes',
        //     price: '£2',
        //     size: '3kg',
        // }
    ],
    page: 1,
    perPage: 4
});

export const getters = {
    getIngredientsPerPage(state, getters){
        let pages = getters.getPages;

        return pages[state.page - 1];
    },
    getPages(state){
        let ingredients = state.ingredients;

        return _.chunk(ingredients, state.perPage);
    },
    getPageNumber(state){
        return _.chunk(state.ingredients, state.perPage).length;
    }
};

export const mutations = {
    SET_INGREDIENT (state, payload) {
        // state.ingredients.push(payload);
        state.ingredients = [payload].concat(state.ingredients);
    },
    SET_INGREDIENTS (state, payload) {
        state.ingredients = payload;
    },
    SET_PAGE(state, page){
        state.page = page;
    },
    INCREMENT_PAGE(state){
        state.page = state.page + 1;
    },
    DECREMENT_PAGE(state){
        state.page--;
    }
};

export const actions = {
    incrementPage({commit, getters, state}){

        if(state.page < getters.getPageNumber){
            commit('INCREMENT_PAGE');
        }
    },
    decrementPage({commit, state}){

        if(state.page > 1){
            commit('DECREMENT_PAGE');
        }
    },
    addIngredient({commit}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.$post('/add-ingredient', {
                name: payload.name,
                price: payload.price,
            }).then((res) => {

                commit('SET_INGREDIENT', res);

                resolve(res);
            }).catch(error => {
                console.log(error);
                reject(error)
            });
        });
    },
    fetchIngredients({commit}){
        return new Promise((resolve, reject) => {
            this.$axios.$get('/get-ingredients')
                .then((res) => {

                    commit('SET_INGREDIENTS', res);
                    resolve(res);

                }).catch(error => {
                    console.log(error);
                    reject(error)
                });
        })

    },
    deleteIngredient({ commit, dispatch }, payload){
        return new Promise((resolve, reject) => {
            this.$axios.$post('/delete-ingredient/' + payload.id)
                .then((res) => {
                    dispatch('fetchIngredients').then(() => {
                        resolve(res);
                    });
                }).catch(error => {
                    console.log(error);
                    reject(error);
                });
        });
    },
    editIngredient({commit, dispatch, state, rootState}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.$post('/edit-ingredient/' + payload.id, payload)
                .then((res) => {
                    console.log(res)

                    dispatch('fetchIngredients').then(() => {
                        resolve(res);
                    });

                }).catch(error => {
                    console.log(error);
                reject(error)
            });
        });
    }
};
