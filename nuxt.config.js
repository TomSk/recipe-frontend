export default {
    loading: false,
    mode: 'universal',
    /*
    ** Headers of the page
    */
    head: {
        title: process.env.npm_package_name || '',
        meta: [
          { charset: 'utf-8' },
          { name: 'viewport', content: 'width=device-width, initial-scale=1' },
          { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
        ],
        link: [
          { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
          { rel: 'stylesheet', href: 'https://unpkg.com/ionicons@4.5.10-0/dist/css/ionicons.min.css' },
          { rel: 'stylesheet', href: 'https://unpkg.com/vue-select@latest/dist/vue-select.css' }
        ]
    },

    env: {
        apiUrl: 'https://recipe-api.datafood.co/api'
    },
    /*
    ** Customize the progress-bar color
    */
    /*
    ** Global CSS
    */
    css: [
        '@/assets/css/main.pcss',
        'quill/dist/quill.snow.css',
        'quill/dist/quill.bubble.css',
        'quill/dist/quill.core.css',

    ],
    /*
    ** Plugins to load before mounting the App
    */
    plugins: [
        '~plugins/vue-js-modal',
        '~/plugins/directives.js',
        {src: '~plugins/date-picker', ssr: false},
        { src: '~plugins/quill-editor.js', ssr: false }
    ],
    /*
    ** Nuxt.js dev-modules
    */
    buildModules: [
        // Doc: https://github.com/nuxt-community/nuxt-tailwindcss
        '@nuxtjs/tailwindcss',

    ],
    /*
    ** Nuxt.js modules
    */
    modules: [
        '@nuxtjs/axios',
        '@nuxtjs/auth',
    ],
    auth: {
        strategies: {
           local: {
                endpoints: {
                    login: { url:'/login', method: 'post', propertyName: 'access_token'},
                    logout: { url:'/logout', method: 'post', propertyName: 'access_token'},
                    user: { url:'/user', method: 'get', propertyName: false}
                },
            },
        },
        localStorage: false,
        cookie: {
            prefix: 'auth.',
            options: {
                path: '/'
            }
        }
    },
    axios: {
        baseURL: 'https://recipe-api.datafood.co/api'
        // baseURL: 'http://127.0.0.1:8000/api'
    },
    /*
    ** Build configuration
    */
    build: {
    /*
    ** You can extend webpack config here
    */
        vendor: [
            'babel-polyfill'
        ],
        babel: {
            sourceType: 'unambiguous',
        },
        extractCSS: true,

        postcss: {
              plugins: {
                  // Add some plugins
                  'postcss-import': {},
                  'postcss-nested': {},
                  'postcss-extend': {},
                }
            },
            extend (config, ctx) {},
    },
    purgeCSS: {
            whitelistPatterns: [/fade/, /slide/, /vs/, /picker/, /quill/, /ql/, /transform/, /trans/],
            whitelist: [
               'dropdown', 'select-list', 'display-time',
                'hours', 'hint', 'minutes', 'clear-btn'
            ],
    }
}
